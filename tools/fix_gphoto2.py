fileP = "/usr/share/dbus-1/services/org.gtk.Private.GPhoto2VolumeMonitor.service"

f = open(fileP, 'r')

content = f.read()
lines = content.split('\n')

for idx in range(0, len(lines)):
    lines[idx] = "#" + lines[idx]

f.close()

f = open(fileP, 'w')
f.write('\n'.join(lines))
f.close()
