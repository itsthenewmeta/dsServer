import win32print
import win32ui
from PIL import Image, ImageWin

HORZRES = 8
VERTRES = 10

LOGPIXELSX = 88
LOGPIXELSY = 90

PHYSICALWIDTH = 110
PHYSICALHEIGHT = 111

PHYSICALOFFSETX = 112
PHYSICALOFFSETY = 113

file_name = "test.jpg"

def printImage(printerN, img, name):
    #printer_name = win32print.GetDefaultPrinter ()
    hDC = win32ui.CreateDC ()
    hDC.CreatePrinterDC (printerN)

    printable_area = hDC.GetDeviceCaps (HORZRES), hDC.GetDeviceCaps (VERTRES)
    printer_size = hDC.GetDeviceCaps (PHYSICALWIDTH), hDC.GetDeviceCaps (PHYSICALHEIGHT)
    printer_margins = hDC.GetDeviceCaps (PHYSICALOFFSETX), hDC.GetDeviceCaps (PHYSICALOFFSETY)

    #bmp = Image.open (file_name)
    #if bmp.size[0] > bmp.size[1]:
    #bmp = bmp.rotate (90)

    ratios = [1.0 * printable_area[0] / img.size[0], 1.0 * printable_area[1] / img.size[1]]
    scale = min (ratios)

    hDC.StartDoc (name)
    hDC.StartPage ()

    dib = ImageWin.Dib (img)
    scaled_width, scaled_height = [int (scale * i) for i in img.size]
    x1 = int ((printer_size[0] - scaled_width) / 2)
    y1 = int ((printer_size[1] - scaled_height) / 2)
    x2 = x1 + scaled_width
    y2 = y1 + scaled_height
    dib.draw (hDC.GetHandleOutput (), (x1, y1, x2, y2))

    hDC.EndPage ()
    hDC.EndDoc ()
    hDC.DeleteDC ()
