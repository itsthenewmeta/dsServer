# -*- coding: utf-8 -*-

import os, sys
import win32print

from PIL import Image, ImageDraw, ImageFont
from time import localtime, gmtime, strftime
import datetime

import jpgprinter

# 102x152mm
space = 40
spaceX = 40
spaceY = 40
size = (1205,1795)
smallSize = (413, 531)
icaoSize = (0, 300, 1728, 2522) # crop
cropFix = (0, 0, 1205, 1795) # crop

arialFont = ImageFont.truetype('fonts/arial.ttf', 42)

def GetTime():
    return strftime("%d/%m/%Y %H:%M", localtime())

def gSize(img):
    (_,_,w,h) = img.getbbox()
    return (w , h)

def cropImage(img, box):
    print "cropImage..."
    return img.crop(box)

def rotateImage(img, val):
    print "rotateImage... %i" % val
    return img.rotate(val,Image.BICUBIC,True) # BICUBIC

def resizeImage(img):
    print "resizeImage..."
    return img.resize(smallSize, Image.ANTIALIAS)

def multiplexImage(to, img):
    print "multiplexImageG..."

    (w , h) = size
    (ww , hh) = gSize(img)

    totalYSpace = (h - (hh * 3)) / 3
    yHl = totalYSpace / 2

    x2Pos = spaceX + space + ww

    to.paste(img,(spaceX ,yHl))
    to.paste(img,(x2Pos  ,yHl))

    to.paste(img,(spaceX ,(totalYSpace * 2) + hh - yHl))
    to.paste(img,(x2Pos  ,(totalYSpace * 2) + hh - yHl))

    to.paste(img,(spaceX ,(totalYSpace * 3) + (hh * 2) - yHl))
    to.paste(img,(x2Pos  ,(totalYSpace * 3) + (hh * 2) - yHl))
    return to

def drawInfo(img, name, info, date):
    print "drawInfo..."
    d = ImageDraw.Draw(img)
    d.text((700, 1500), name, fill='black', font=arialFont)
    d.text((700, 1550), info, fill='black', font=arialFont)
    d.text((700 + 2, 1600), date, fill='black', font=arialFont)

def drawInfoG(img, name, info, date):
    print "drawInfoG..."
    img = rotateImage(img, 270)

    (x,y) = gSize(img)
    y = (y / 5) * 4

    d = ImageDraw.Draw(img)
    d.text((40, y), name, fill='black', font=arialFont)
    d.text((40, y + 60), info, fill='black', font=arialFont)
    d.text((40 + 2, y + 120), date, fill='black', font=arialFont)

    img = rotateImage(img, -270)

    return img

def run():
    print "loading..."
    i_out = Image.new("RGB", size, "white")
    i_base = Image.open("test.jpg")
    i_out = multiplexImage(i_out, resizeImage(cropImage(rotateImage(i_base,270), icaoSize)))

    i_out = drawInfoG(i_out, u"Belen Çiloğulları", "DreamShot 360",GetTime() )

    print "saving..."
    i_out = cropImage(i_out,cropFix)
    i_out.save("out.jpg")
    print "printing..."
    jpgprinter.printImage("KODAK 305 Photo Printer",i_out, "DS360 - OUT")

run()

#p = win32print.OpenPrinter ("KODAK 305 Photo Printer")
