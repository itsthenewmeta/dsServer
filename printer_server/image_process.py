from PIL import Image, ImageDraw, ImageFont
import os

# 102x152mm
size = (1205,1795)

defaultPDF = "t.pdf"

baseIMG = Image.new("RGB", size, "white")
img = Image.open("test.jpg")


croppedimg = img.crop((0, 300, 1728, 2522))
croppedimg.save("cropped.jpg")



img2 = croppedimg.resize((413, 531), Image.ANTIALIAS)
img2.save("resize.jpg")

imgBox = img2.getbbox()

baseIMG.paste(img2,(40,40))
baseIMG.paste(img2,(600,40))
baseIMG.paste(img2,(40,600))
baseIMG.paste(img2,(600,600))


draw = ImageDraw.Draw(baseIMG)
fontsFolder = 'fonts/' # e.g. 'Library/Fonts'
arialFont = ImageFont.truetype(os.path.join(fontsFolder, 'arial.ttf'), 32)
draw.text((700, 1500), 'Belen Cilogullari', fill='black', font=arialFont)
draw.text((700, 1550), 'Deneme', fill='black', font=arialFont)
draw.text((700, 1600), 'Bisiler', fill='black', font=arialFont)

baseIMG.save("out.jpg")
