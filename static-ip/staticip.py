import os
import shutil


# not working


filePath = "/etc/network/interfaces"

routeFile = "route.data"
dConfig = "default.config"

#filePath = "misc.py"

address  = "192.168.1.111"

defaultSetting = "auto lo\niface lo inet loopback\nauto eth0\niface eth0 inet static\naddress " + address + "\ngateway 192.168.1.1\nnetmask 255.255.255.0\nnetwork 192.168.1.0\nbroadcast 192.168.1.255"

def main():
    if(makeBackup()):
        writeSettings()
        restartSys()

def makeBackup():
    backupFilePath = filePath + ".old"
    if(os.path.exists(filePath)):
        if(os.path.exists(backupFilePath)):
            print "Backup file made."
            shutil.copy(filePath, backupFilePath)
            return True
        else:
            print "Backup already exists. Delete backup at to continue : " + backupFilePath
            return False
    else:
        print "There is no file to backup."
        return False

def getKVal(src,key):
    src = src.split()[4:]
    for x in range(1,8):
        if(src[x] == key):
            return (src[x + 8], src[x + 16])

    return ("","")

def writeSettings():
    target = open(filePath, 'w')
    target.truncate();
    target.write(defaultSetting)
    target.close()
    print "Done."


def testRead():
    fHandle = open(routeFile, 'r')
    content = fHandle.read()

    print getKVal(content,"Gateway")

def restartSys():
    os.system('sudo reboot')

testRead()
