import os, sys import win32print

from PIL import Image

# 102x152mm
size = (1205,1795)

defaultPDF = "t.pdf"

baseIMG = Image.new("RGB", size, "white")
img = Image.open("IMG_0495.jpg")


croppedimg = img.crop((0, 300, 1728, 2522))
croppedimg.save("cropped.jpg")



img2 = croppedimg.resize((413, 531), Image.ANTIALIAS)
img2.save("resize.jpg")

imgBox = img2.getbbox()

baseIMG.paste(img2,(40,40))
baseIMG.paste(img2,(600,40))
baseIMG.paste(img2,(40,600))
baseIMG.paste(img2,(600,600))

baseIMG.save("out.jpg")


p = win32print.OpenPrinter (KODAK 305 Photo Printer)
