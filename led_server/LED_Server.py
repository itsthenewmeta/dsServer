from Adafruit_PWM_Servo_Driver import PWM
from flask import Flask

app = Flask(__name__)

pMin = 0
pMax = 4096

ok = "confirmed"

pwm = PWM(0x40)
pwm.setPWMFreq(1000)

@app.route('/')
def info():
    return "-ok\nled_server\n0.4.2"

@app.route('/<channel>/on')
def openLED(channel):
    if(channel == 0):
        pwm.setPWM(0, pMax, pMin)
    return ok

@app.route('/<channel>/off')
def closeLED(channel):
    if(channel == 0):
        pwm.setPWM(0, pMin, pMax)
    return ok

@app.route('/<channel>/dim/<value>')
def dimLED(channel,value):
    if(channel == 0):
        pwm.setPWM(0, pMin + value,value - pMax)
    return ok

app.run(host = "0.0.0.0",port = 8081)
