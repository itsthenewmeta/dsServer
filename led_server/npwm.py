from Adafruit_PWM_Servo_Driver import PWM
import time

pMin = 0
pMax = 4096
pTes = 1024

timew = 3

pwm = PWM(0x40)
pwm.setPWMFreq(200)

def runTest():
	while (True):
		openLED(1);
		dimLoop(3, 1 , int(pMax * .1) +1)
		closeLED(1)

def dim(val,w):
	newVal = pMax -val
	print("    Dim to -> {0} ~ {1}".format(val , newVal))
	#pwm.setPWM(0, 0, val)
	pwm.setPWM(0, val,newVal)
	time.sleep(w)

def dimLoop(w,dps, sens):
    print "Dim to loop started. DPS(Dim Per Second) = %i" % dps
    val = pMax
    while(val >= sens):
		dim(val,dps)
		val -= sens
   
    print "Dim to loop ended. Waiting for sec -> %i" % w
    time.sleep(w)
    
def openLED(w):
    print "Open LED for sec -> %i" % w
    print "Open LED for sec -> %i" % (4096/512)
    print "%d" % (2048/1280)
    #pwm.setPWM(1, pMax, pMin)
    pwm.setPWM(0, pMax, pMin)
    time.sleep(2)
    while(True):
		print "Ac"
		pwm.setPWM(0, 1,0)
		time.sleep(2)
		print "Loop"
		pwm.setPWM(0, 1280,2048)
		time.sleep(2)
		print "Loop2"
		pwm.setPWM(0, 1280,2048)
		time.sleep(2)
		print "Loop3"
		pwm.setPWM(0, 512,1024)
		time.sleep(2)
		print "Loop4"
		pwm.setPWM(0, 412,512)
		time.sleep(2)
		
def closeLED(w):
    print "Close LED for sec -> %i" % w
    pwm.setPWM(0, pMax, pMin)
    pwm.setPWM(1, pMin, pMax)
    time.sleep(w)

runTest()
