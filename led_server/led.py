from Adafruit_PWM_Servo_Driver import PWM

pMin = 0
pMax = 4096

pwm = PWM(0x40)
pwm.setPWMFreq(1000)

def openLED():
    pwm.setPWM(0, pMax, pMin)

def closeLED():
    pwm.setPWM(0, pMin, pMax)
