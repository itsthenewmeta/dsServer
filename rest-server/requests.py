import settings
import misc

import glob

from flask import send_file

streamRunning = True

def index():
    return "App is running"

def take_picture(): # take picture and return stdout
    out = misc.runProc(settings.cmd_base + settings.p_take_image)
    fileName = misc.findFileNameByExt(out,"JPGDeleting")[:-8]

    print(fileName)
    if(fileName != 'None'):
        #settings.update_l_picture(fileName)
        return send_file(fileName)
    else:
        return 'Camera Error'

def get_preview(): # take and return preview
    #set_setting("eosremoterelease","5")
    misc.runProc(settings.cmd_base + settings.p_take_preview)
    #misc.runSCmd(settings.cmd_base, settings.p_take_preview);

    return send_file("thumb_preview.jpg")

def stream():
    return send_file("stream.jpg")

def getLastPreview():
        files = glob.glob('stream/*.jpg')

        files.sort()

        last = False

        if (files):
            last = files.pop()
            print(last)
            return send_file(last)
        return send_file("stream01.jpg")

def get_picture(): # get last taken picture -- not working right now
    return send_file("pic.jpeg")

#def stream():
#    while streamRunning:
#        misc.runProc(settings.cmd_base + settings.p_take_preview)
#        yield send_file "preview.jpg"
