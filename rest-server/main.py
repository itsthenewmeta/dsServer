import requests
import settings
import misc

from flask import Flask

app = Flask(__name__)

@app.route('/setting/<setting>')
def get_setting(setting):
    out = misc.runProc(settings.cmd_base + settings.p_get_setting + setting)

    return out

@app.route('/setting/<setting>/<value>')
def set_setting(setting, value):
    out = misc.runProc(settings.cmd_base + settings.p_set_setting + setting + '=' + value)

    return out

def init_run(m_app):
    #misc.runCmd("killall PTPCamera") // only for mac
    #settings.load_settings() #settings.update_setting('port',8080)

    misc.runProc(settings.cmd_init) # gphoto2 --auto-detect

    m_app.add_url_rule('/',            'index',       requests.index)
    m_app.add_url_rule('/get_picture', 'get_picture', requests.get_picture)
    m_app.add_url_rule('/get_preview', 'get_preview', requests.get_preview)
    m_app.add_url_rule('/take_picture','take_picture',requests.take_picture)
    m_app.add_url_rule('/stream'       ,'stream'     ,requests.getLastPreview)

    m_app.run(host = "0.0.0.0",port = 8080) #settings.g_port

init_run(app)
