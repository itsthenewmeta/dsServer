import subprocess

def runProc(proc):
    return subprocess.check_output(proc,shell=True)

def runCmd(cmd):
    return subprocess.call(cmd,shell=True)

def runSCmd(cmd,param):
    return subprocess.call(['sudo', cmd, param])

def findFileNameByExt(str,ext):
    #print("\nDBG_____OUT_START \n")
    #print(str)
    w = str.split('\n')
    words =  unifi(w).split(' ')

    #print(words)
    #print("\nDBG_____OUT_END \n")

    for word in words:
        if(word[-len(ext):] == ext):
            return word

    return 'None'

def unifi(arr):
	data = ""
	for a in arr:
		data += a
	return data
